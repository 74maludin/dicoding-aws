## Belajar Dasar AWS Cloud

### 1. Pengantar ke AWS
- Cloud computing (_on demand_, _pas as you go_)
  - Penggunaan sesuai kebutuhan
  - Sumber daya IT
  - Melalui internet
- Model penerapan komputasi Cloud
  - _Cloud based deployment_
  - _On-premises deployment_
  - _Hybrid deployment_
- Manfaat komputasi Cloud
  - Ubah pengeluaran di muka menjadi pengeluaran variabel
  - Hentikan biaya pengelolaan dan pemeliharaan data center
  - Berhenti menebak kapasitas
  - Manfaatkan skala ekonomi yang masif
  - Tingkatkan kecepatan & ketangkasan
  - Mendunia dalam hitungan menit

### 2. Komputasi Cloud
- 📢 Pengenalan Amazon Elastic Compute Cloud (EC2)
  - Server virtual disebut _instance_
  - Hypervisor : _multitenancy_
  - _compute as a service_
- Tipe Instance Amazon EC2
  - _General purpose instances_
  - _Compute optimized instances_
  - _Memory optimized instances_
  - _Accelerated computing instances_
  - _Storage optimized instance_
- Harga Amazon EC2
  - _On demand_
  - _Saving plans_ { fargate, lambda }
  - _Reserved instances_
  - _Spot instances_
  - _Dedicated host_
- Penyesuaian Kapasitas Amazon EC2 (skala & elastisitas)
  - Amazon EC2 Auto Scaling
    - _Scaling up_ -> sumber daya mesin
    - _Scaling out_ -> menambah _instance_
    - _Scaling down_
    - _Scaling in_
  - Auto Scaling Group
    - _Minimum capacity_
    - _Desired capacity_
    - _Maximum capacity_
- 📢 Traffic dengan Elastic Load Balancing (ELB) - penyeimbang beban
  - Elastic Load Balancing
    - _Regional construct_
- Messaging & Queueing
  - _buffer_ == antrean
  - _coupled architecture_ == monolitik
  - layanan
    - 📢 Amazon Simple Queue Service (SQS)
    - 📢 Amazon Simple Notification Service (SNS)
- Layanan Komputasi Tambahan
  - Komputasi _serverless_
    - 📢 AWS Lambda
  - Container
    - 📢 Amazon Elastic Container Service (ECS) _in_ EC2
    - 📢 Amazon Elastic Kubernetes Service (EKS) _in_ EC2
    - 📢 Amazon Fargate

### 3. Infrastruktur Global & Keandalan
_High availability_ (ketersediaan tinggi) \
_Fault tolerance_ (toleransi terhadap kesalahan)
- Infrastruktur Global AWS
  - AWS Region, pemilihan
    - _compliance_
    - _proximity_
    - _feature availability_
    - _pricing_
  - Availability zone
- Edge Location
  - 📢 Amazon Cloudfront (cache, _Content Delivery Network_ (CDN))
  - 📢 AWS Outposts, region mini - data center sendiri-.
- Cara menyediakan sumber daya AWS
  - AWS Management Console
  - AWS Command Line Interface
  - AWS Software Development Kit
  - 📢 AWS Elastic Beanstalk (kode & konfigurasi)
  - 📢 AWS Cloud Formation
    - infrastruktur as code
    - deklaratif berbasis JSON / YAML

### 4. Jaringan
- Konektivitas ke AWS
  - 📢 Amazon Virtual Private Cloud (VPC)
  - _Internet gateway_
  - Virtual private gateway == VPN
  - AWS Direct Connect
- Subnet & Network Access Control List
  - Subnet (pengelompokan sumber daya VPC)
- Network Access Control List == _stateless_
  - Keamanan AWS
    - _Network hardening_
    - Keamanan aplikasi
    - Identitas pengguna
    - Autentikasi & Otorisasi
    - Pencegahan DDOS
    - Integritas Data
    - Enkripsi, dll
  - Security Group == _statefull_
- Jaringan global
  - Domain Name System (DNS) == buku alamat
    - 📢 Amazon Route 53
    - Amazon Cloudfront
      - AWS web application firewall
      - AWS certificate manager
      - Amazon S3, Route 53, dll

### 5. Penyimpanan dan Database
- 📢 Instance Store & Amazon Elastic Block Store (EBS)
  - Tipe hard drive
    - Instance store
    - EBS
- 📢 Amazon Simple Storage Service (S3)
  - S3 Standard
  - S3 Standard = Infrequent Access
  - S3 Standard One Zone - Infrequent Access
  - S3 Intelligent Tiering
  - S3 Glacier
  - S3 Glacier Deep Archive
- 📢 Amazon Elastic File System (EFS)
- 📢 Amazon Relational Database Service (RDS)
  - Amazon Aurora
  - PostgreSQL
  - MySQL
  - MariaDB
  - Oracle Database
  - Microsoft SQL Server
- 📢 Amazon DynamoDB
- 📢 Amazon Redshift - data warehouse
- AWS Database Migration -- AWS Schema Convenien Tool
- Layanan database tambahan
  - 📢 Amazon documentDB -- mongodb
  - 📢 Amazon Neptune -- graph database
  - 📢 Amazon Managed Blockchain
  - 📢 Amazon Quantum Ledger Database (QLDB)
  - Akselerator database
    - 📢 Amazon ElastiCache
    - 📢 Amazon DynamoDB Accelerator (DAS)
    - Redis & Memcached (Solr)

### 6. Keamanan
AWS - _security of the cloud_ \
Client - _security in the cloud_
- Shared Responsibility Model
  - AWS
    - Physical
    - Network & Hypervisor
  - Client
    - Operating System
- Perizinan & Hak Akses Pengguna
  - root - MFA
  - 📢 AWS Identity and Access Management (AWS IAM)
    - IAM Users
    - IAM Policies
    - IAM Groups
    - IAM Roles
- AWS Organizations
  - Manajemen terpusat
  - _Consolidated billing_
  - Pengelompokan hierarki akun
  - Kontrol atas layanan AWS & tindakan API
- Compliance
  - 📢 AWS Artifact
    - AWS Artifact Agreements
    - AWS Artifact Reports
  - Costumer Compliance Center
- Serangan Denial of Service
  - Serangan Distributed Denial of Service (DDOS)
    - (Tipe) UDP Flood
    - HTTP level attack
    - Slowloris attack
  - 📢 AWS Shield
    - standard
    - advanced
  - 📢 AWS Web Application Firewall (WAF)
- Layanan keamanan tambahan
  - enkripsi
    - enkripsi _at rest_
    - enkripsi _in transit_
  - 📢 AWS Key Management Service (AWS KMS)
    - _cryptographic key_
  - 📢 Amazon Inspector
  - 📢 Amazon GuardDuty

### 7. Pemantauan dan Analitik
- 📢 Amazon Cloudwatch
  - metrik, batas, alarm
  - akses ke semua metrik dari satu lokasi
  - visibilitas ke seluruh aplikasi, infrastruktur, dan layanan
  - mengurangi waktu MTTR dan meningkatkan TCO
  - mengoptimalkan aplikasi dan sumber daya operasional
- 📢 AWS CloudTrail
  - audit API, log / jejak tindakan
- 📢 AWS Trusted Advisor
  - rekomendasi praktik
  - alert : tanpa masalah, investigasi, dan tindak lanjut
  - pilar layanan
    - _cost optimization_
    - performance
    - security
    - _fault tolerance_
    - service limits

### 8. Harga dan Dukungan
- 📢 AWS Free Tier
  - Always free
  - 12 Months free
  - Trials
- Konsep harga AWS
  - _pay for what you use_
  - _pay less when you reserve_
  - _pay less with volume-based discounts when you use more_
  - AWS pricing calculator
- Billing dashboard
- Consolidated billing
- 📢 AWS Budgets
- 📢 AWS Support plans
  - basic
  - developer
  - business
  - enterprise
- 📢 AWS Marketplace

### 9. Migrasi dan Inovasi
- 📢 AWS Cloud Adoption Framework (AWS CAF)
  - Perspektif
    - Business
    - People
    - Governance
    - Platform
    - Security
    - Operations
- Strategi Migrasi
  - _Rehosting_ -- lift and shift
  - _Replatforming_ -- lift, tinker, and shift
  - _Retiring_
  - _Retaining_
  - _Repurchasing_
  - _Refactoring / re-architecting_
- 📢 AWS Snow Family
  - 📢 AWS Snowcone
  - 📢 AWS Snowball
    - _Snowball edge storage optimized_
    - _Snowball edge compute optimized_
  - 📢 AWS Snowmobile
- Inovasi dengan AWS
  - Serverless application
  - Artificial Inteligence
    - 📢 Amazon Transcribe : speech to text
    - 📢 Amazon Comprehend : NLP
    - 📢 Amazon Fraud Detector : aktivitas online -> penipuan
    - 📢 Amazon Lex : chatbot
    - 📢 Amazon Textract : proses teks, form table, dll
  - Machine Learning
    - 📢 Amazon Sagemaker
    - 📢 Amazon Deepracer : _reinforcement learning_
  - 📢 AWS Ground Station : satelit

### 10. Perjalanan Cloud
- 📢 AWS Well-Architected Framework
  - operational excellence
  - security
  - reliability
  - performance efficiency
  - cost optimization

### 11. Dasar-dasar AWS Certified Cloud Practicioner
- Detail Ujian
  <table>
    <tr>
        <th>Materi</th>
        <th>Bobot</th>
    </tr>
    <tr>
        <td>Cloud Concepts</td>
        <td>26%</td>
    </tr>
    <tr>
        <td>Security and Compliance</td>
        <td>25%</td>
    </tr>
    <tr>
        <td>Technology</td>
        <td>33%</td>
    </tr>
    <tr>
        <td>Billing and Pricing</td>
        <td>16%</td>
    </tr>
  </table>